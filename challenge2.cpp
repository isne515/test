#include<iostream>

using namespace std;

void mark1 (int[][7], int); 
void mark2 (int[][7], int); 
void table(int[][7]); 
int check1(int a[][7]);
int check2(int a[][7]);
int check3(int a[][7]);
int check4(int a[][7]);

int main(){
	int columnrow[6][7]= {}; //array function 
	int col,x,player=1 , a=1; // variable for recieve value(col), for display who win(player)  
	
	cout << "Player 1 = 'X' " << endl;
	cout << "Player 2 = 'O' " << endl;
	
	do{

	cout << endl;
	player=1;
	cout << "Player 1 :";
	cin>>col;	
	mark1(columnrow,col);
	table(columnrow);
	if(check1(columnrow)==0){
		break;
	}
	if(check2(columnrow)==0){
		break;
	}
	if(check3(columnrow)==0){
		break;
	}
	if(check4(columnrow)==0){
		break;
	}
	

	player=2;
	cout << "Player 2 :";
	cin>>col;
	mark2(columnrow,col);
	table(columnrow);
	if(check1(columnrow)==0){
		break;
	}
	if(check2(columnrow)==0){
		break;
	}
	if(check3(columnrow)==0){
		break;
	}
	if(check4(columnrow)==0){
		break;
	}
	
	}while(x==0);


cout << "Player " << player << " Win!" << endl;	
	return 0;
}
void mark1 (int grid[][7], int column){ //print 'x' function
	column=column-1;
	for(int i=5; i>=0; i--){
		if(grid[i][column] == 0){
			grid[i][column] = 1;		
		break;
		}	
	}
}

void mark2 (int grid[][7], int column){ //print 'O' function
	column=column-1;
	for(int i=5; i>=0; i--){
		if(grid[i][column] == 0){
			grid[i][column] = 2;		
		break;
		}
	}
}

void table(int a[][7]){ //print gametable function
	
		for(int i=0; i<6; i++){
			for(int j=0; j<7; j++){
				cout<<"|";
	if( a[i][j] == 1){
		cout << "X";
	}else if( a[i][j] == 2 ){
		cout << "O";
	}else if( a[i][j] == 0 ){
		cout << "_";
	}cout<<"|";
		}			cout << endl;

		}
		cout << "=====================" ;
		cout << "\n 1  2  3  4  5  6  7" << endl;
		cout << endl;	
	}

int check1(int a[][7]){ //check horizontal line function
	
	for(int i=5; i>=0; i--){
		for(int j=0; j<7; j++){
			if(a[i][j]!=0 && a[i][j+1]==a[i][j] && a[i][j+2]==a[i][j] && a[i][j+3]==a[i][j]){
				return 0;
			}
		}
	}return -1;
}

int check2(int a[][7]){ //check vertical line function
	for(int j=6; j>=0; j--){
		for(int i=5; j>=0; j--){
			if(a[i][j]!=0 && a[i-1][j]==a[i][j] && a[i-2][j]==a[i][j] && a[i-3][j]==a[i][j]){
				return 0;
			}
		}
	}return -1;	
}

int check3(int a[][7]){ //check left diagonal line function
	
	for(int i=0; i<=5; i++){
		for(int j=0; j<=5; j++){
			if(a[i][j]!=0 && a[i+1][j+1]==a[i][j] && a[i+2][j+2]==a[i][j] && a[i+3][j+3]==a[i][j]){
				return 0;
			}
		}
	}return -1;
}

int check4(int a[][7]){ //check right diagonal line function
	
	for(int i=0; i<=5; i++){
		for(int j=0; j<=6; j++){
			if(a[i][j]!=0 && a[i-1][j+1]==a[i][j] && a[i-2][j+2]==a[i][j] && a[i-3][j+3]==a[i][j]){
				return 0;
			}
		}
	}return -1;
}
